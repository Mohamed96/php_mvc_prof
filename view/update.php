<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
        <?php print htmlentities($title) ?>
    </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.min.css"/>
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
</head>
<body>
<?php
     

/*if ( $errors ) {
     print '<ul class="errors">';
     foreach ( $errors as $field => $error ) {
         print '<li>'.htmlentities($error).'</li>';
     }
     print '</ul>';
 }*/
?>
<div class="container col-md-4  col-md-offset-3 test">
    <div class=" panel panel-info">
        <div class=" panel-heading"><center><strong> Modification Contact </strong</center>  </div>
        <div class="panel-body">
            <form method="POST" action="">
                <div class="panel panel-body">
                    <div class="form-group">
                        <label for="name" class="control-label">Nom:</label>
                        <input class="form-control" type="text" name="name"  value= "<?php  print htmlentities($name) ?>"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="phone">Telephone:</label>
                            <input class="form-control" type="text" name="phone" required value="<?php print htmlentities($phone) ?>"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="email">Email:</label>
                            <input class="form-control" type="email" name="email"  required value="<?php print htmlentities($email) ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="address">Adresse:</label>
                            <input  class="form-control" name="address" type="text" "/>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="form-submitted" value="1" />
                            <input class="btn btn-success" type="submit" value="Modifier"/>
                        </div>
                        <a class="btn btn-primary" href="index.php"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
                    </div>

            </form>
        </div>
    </div>
</div>
</body>
</html>

