<!DOCTYPE HTML>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Authentification</title>
    <link rel="stylesheet" href="../css/style.css"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../css/bootstrap-cerulean.min.css"/>
</head>
<body>


<div class="container col-md-4 col-md-offset-4 test">
    <div class="panel panel-danger">
        <div class=" panel-heading"><center><strong>AUTHENTIFICATION</strong></center> </div>
        <div class=" panel-body">
            <form method="post" action="../controller/loginController.php" >
                <div class=" panel-body">
                <div class="form-group">
                    <label class="control-label"> USERNAME</label>
                    <input class="form-control" type="text" name="username"/>
                </div>
                <div class="form-group">
                    <label class="control-label"> PASSWORD</label>
                    <input class="form-control" type="password" name="password"/>
                </div>

                <input type="submit" name="op" class="btn btn-info" value="Se connecter"/>

        </div>
                <?php
                if (@$_GET['err']==1 ) {?>
               <div class="alert-danger"> données incorrectes.
                  veillez recommencer!</div>
                <?php } ?>
        </form>
    </div>
</div>
</div>
</body>
</html>